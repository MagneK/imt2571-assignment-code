<?php

include_once("Book.php");
include_once("IModel.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Model implements IModel
{			
	/**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8mb4', 'User1', 'UndercoverOperative1');
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
    }	
	
	/** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		$stmt = $this->db->query('SELECT * FROM Book ORDER BY id');
		$result = $stmt->fetchAll(PDO::FETCH_NUM);
		
		$rows = $stmt->rowCount();
		
		for ($i = 0; $i < $rows; $i++)
		{
			$booklist[$i] = new Book($result[$i][1], $result[$i][2], $result[$i][3], $result[$i][0]);
		}
		echo sizeof($booklist);
		
        return $booklist;
    }
	
	/** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$stmt = $this->db->query('SELECT * FROM Book WHERE id=' . $id);
		$result = $stmt->fetchAll(PDO::FETCH_NUM);
		if ($result == FALSE)
			return null;
		$book = new Book($result[0][1], $result[0][2], $result[0][3], $result[0][0]);
        return $book;
    }
	
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
	public function addBook($book)
	{
		$this->db->query('INSERT INTO Book(title, author, description) VALUES(\'' . $book->title . '\', \'' . $book->author . '\', \'' . $book->description . '\')');
		$book->id = $this->db->lastInsertId();
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
	public function modifyBook($book)
	{
		$this->db->query('UPDATE Book SET title=\'' . $book->title . '\', author=\'' . $book->author . '\', description=\'' . $book->description . '\' WHERE id=' . $book->id);
	}

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
	public function deleteBook($id)
	{
		$this->db->query('DELETE FROM Book WHERE id=' . $id);
	}

}

?>