<?php

include_once("Book.php");
include_once("IModel.php");
include_once("DBProps.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{			
	/**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try
			{
				$this->db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PWD);
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch(PDOException $e)
			{
				error_log($e->getMessage());
			}
		}
    }	
	
	/** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id; null incase of error.
     */
    public function getBookList()
    {
		try
		{
			$booklist = array();
			$stmt = $this->db->query('SELECT * FROM Book ORDER BY id');
			$result = $stmt->fetchAll(PDO::FETCH_NUM);
			
			$rows = $stmt->rowCount();
			
			for ($i = 0; $i < $rows; $i++)
			{
				$booklist[$i] = new Book($result[$i][1], $result[$i][2], $result[$i][3], $result[$i][0]);
			}
			
			return $booklist;
		}
		catch(PDOException $e)
		{
			error_log($e->getMessage());
			return null;
		}
    }
	
	/** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     */
    public function getBookById($id)
    {
		try
		{
			// Show error
			if (!$this->isValidId($id)) 
				return null;
			
			$stmt = $this->db->prepare('SELECT * FROM Book WHERE id=:id');
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			
			$result = $stmt->fetchAll(PDO::FETCH_NUM);
			if ($result == FALSE)
				return null;
			$book = new Book($result[0][1], $result[0][2], $result[0][3], $result[0][0]);
			return $book;
		}
		catch (PDOException $e)
		{
			error_log($e->getMessage());
			return null;
		}
    }
	
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @return TRUE|FALSE True if everything went smoothly; otherwise false;
     */
	public function addBook($book)
	{
		try
		{
			// Show error
			if (!$this->isValidInput($book)) 
				return FALSE;
			
			$stmt = $this->db->prepare('INSERT INTO Book(title, author, description) VALUES(:title, :author, :desc)');
			$stmt->bindValue(':title', $book->title);
			$stmt->bindValue(':author', $book->author);
			$stmt->bindValue(':desc', $book->description);
			$stmt->execute();
			$book->id = $this->db->lastInsertId();
			return TRUE;
		}
		catch(PDOException $e)
		{
			error_log($e->getMessage());
			return FALSE;
		}
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
	 * @return TRUE|FALSE True if everything went smoothly; otherwise false;
     */
	public function modifyBook($book)
	{
		try
		{
			// Show error
			if (!($this->isValidId($book->id) && $this->isValidInput($book)))
				return FALSE;
			
			$stmt = $this->db->prepare('UPDATE Book SET title=:title, author=:author, description=:desc WHERE id=:id');
			$stmt->bindValue(':title', $book->title);
			$stmt->bindValue(':author', $book->author);
			$stmt->bindValue(':desc', $book->description);
			$stmt->bindValue(':id', $book->id);
			$stmt->execute();
			return TRUE;
		}
		catch(PDOException $e)
		{
			error_log($e->getMessage());
			return FALSE;
		}
	}

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
	 * @return TRUE|FALSE True if everything went smoothly; otherwise false;
     */
	public function deleteBook($id)
	{
		try
		{
			// Show error
			if (!$this->isValidId($id)) 
				return FALSE;
			
			$stmt = $this->db->prepare('DELETE FROM Book WHERE id=:id');
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			return TRUE;
		}
		catch(PDOException $e)
		{
			error_log($e->getMessage());
			return FALSE;
		}
	}

	/** Checks if the book id is a valid numeric value greater than -1
	 * @param $id integer The id that is to be checked for validation.
	 * @return true|false True if the $id is valid; false otherwise.
	 */
    private function isValidId($id)
	{
		if (is_numeric($id) && $id > -1)
			return TRUE;
		return FALSE;
	}
	
	/** Checks if the book has a non-empty title and author.
	 * @param $book Book The book that is to be checked for validation.
	 * @return true|false True if both the title and author is valid; false otherwise.
	 */
	private function isValidInput($book)
	{
		if (strlen($book->title) > 0 && strlen($book->author) > 0)
			return TRUE;
		return FALSE;
	}
}

?>